# TER - Espace personnel de données

## Dependencies

npm    -> sudo apt-get install npm
nodejs -> curl -sL https://deb.nodesource.com/setup_14.x | sudo -E bash -
       -> sudo apt-get install -y nodejs

Si vous voulez vérifier que nodejs est installé : node -v


## Getting started

Il faut avoir un pod que l'on a créé à partir du site https://solidcommunity.net/ (il faut se faire un compte).


### Utilisation

Tout d'abord, mettez-vous dans le dossier solid, puis faites les installations nécessaires :
```
npm install
```

Vous pouvez lancer le code avec la commande suivante :
```
npm run build && npm run start
```

### Interface

Pour pouvoir utiliser toutes les fonctions de notre application, vous devez vous connecter à Solid.
