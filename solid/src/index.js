
import {
    getSolidDataset,
    getThing,
    saveSolidDatasetAt,
    overwriteFile,
    getSourceUrl,
    saveFileInContainer,
    addUrl,
    addStringNoLocale,
    createSolidDataset,
    createThing,
    getPodUrlAll,
    getThingAll,
    getUrl,
    getStringNoLocale,
    setStringNoLocale,
    removeThing,
    setThing,
    createContainerAt,
    getFile

} from "@inrupt/solid-client";
import { Session } from "@inrupt/solid-client-authn-browser";
import { VCARD } from "@inrupt/vocab-common-rdf";
import { getDefaultSession } from "@inrupt/solid-client-authn-browser";
import * as solidClient from "@inrupt/solid-client";
const { default: dataFactory } = solidClient;
const { putFile } = solidClient;




/* Import fullCalendar */
import { Calendar } from '@fullcalendar/core';
import interactionPlugin from '@fullcalendar/interaction';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGridPlugin from '@fullcalendar/timegrid';
import listPlugin from '@fullcalendar/list';
import './index.css';
import frLocale from '@fullcalendar/core/locales/fr';

// If your Pod is *not* on `solidcommunity.net`, change this to your identity provider.

// document.getElementById(
//     "solid_identity_provider"
// ).innerHTML = `[<a target="_blank" href="${SOLID_IDENTITY_PROVIDER}">${SOLID_IDENTITY_PROVIDER}</a>]`;

const NOT_ENTERED_WEBID =
    "...not logged in yet - but enter any WebID to read from its profile...";

const session = new Session();

const buttonLogin = document.getElementById("btnLogin");
const writeForm = document.getElementById("writeForm");
const readForm = document.getElementById("readForm");
const foldersForm = document.getElementById("foldersForm");
const filesForm = document.getElementById("filesForm");
const uploadForm = document.getElementById("uploadForm");

const mainFolder = document.getElementById("fileInput");
const folderUrl = document.getElementById("mediaTypeU");
const displayCalendar = document.getElementById("btnCal");
const CalForm = document.getElementById("CalForm");


const SOLID_IDENTITY_PROVIDER = document.getElementById("identityProvider");

// console.log(SOLID_IDENTITY_PROVIDER);

// 1a. Start Login Process. Call session.login() function.
async function login() {
    // if (!session.info.isLoggedIn) {
    await session.login({
        oidcIssuer: SOLID_IDENTITY_PROVIDER.value,
        clientName: "Inrupt tutorial client app",
        redirectUrl: window.location.href
    });
    // }
}

// 1b. Login Redirect. Call session.handleIncomingRedirect() function.
// When redirected after login, finish the process by retrieving session information.
async function handleRedirectAfterLogin() {
    await session.handleIncomingRedirect(window.location.href);
    if (session.info.isLoggedIn) {
        // Update the page with the status.
        document.getElementById(
            "labelStatus"
        ).innerHTML = `Your session is logged in with the WebID [<a target="_blank" href="${session.info.webId}">${session.info.webId}</a>].`;
        document.getElementById("labelStatus").setAttribute("role", "alert");
        document.getElementById("webID").value = session.info.webId;
    }
}

// The example has the login redirect back to the index.html.
// This calls the function to process login information.
// If the function is called when not part of the login redirect, the function is a no-op.
handleRedirectAfterLogin();

// 2. Write to profile
async function writeProfile() {
    const name = document.getElementById("input_name").value;

    if (!session.info.isLoggedIn) {
        // You must be authenticated to write.
        document.getElementById(
            "labelWriteStatus"
        ).textContent = `...you can't write [${name}] until you first login!`;
        document.getElementById("labelWriteStatus").setAttribute("role", "alert");
        return;
    }
    const webID = session.info.webId;
    // The WebID can contain a hash fragment (e.g. `#me`) to refer to profile data
    // in the profile dataset. If we strip the hash, we get the URL of the full
    // dataset.
    const profileDocumentUrl = new URL(webID);
    profileDocumentUrl.hash = "";

    // To write to a profile, you must be authenticated. That is the role of the fetch
    // parameter in the following call.
    let myProfileDataset = await getSolidDataset(profileDocumentUrl.href, {
        fetch: session.fetch
    });

    // The profile data is a "Thing" in the profile dataset.
    let profile = getThing(myProfileDataset, webID);

    // Using the name provided in text field, update the name in your profile.
    // VCARD.fn object is a convenience object that includes the identifier string "http://www.w3.org/2006/vcard/ns#fn".
    // As an alternative, you can pass in the "http://www.w3.org/2006/vcard/ns#fn" string instead of VCARD.fn.
    profile = setStringNoLocale(profile, VCARD.fn, name);

    // Write back the profile to the dataset.
    myProfileDataset = setThing(myProfileDataset, profile);

    // Write back the dataset to your Pod.
    await saveSolidDatasetAt(profileDocumentUrl.href, myProfileDataset, {
        fetch: session.fetch
    });

    // Update the page with the retrieved values.
    document.getElementById(
        "labelWriteStatus"
    ).textContent = `Wrote [${name}] as name successfully!`;
    document.getElementById("labelWriteStatus").setAttribute("role", "alert");
    document.getElementById(
        "labelFN"
    ).textContent = `...click the 'Read Profile' button to to see what the name might be now...?!`;
}

// 3. Read profile
async function readProfile() {
    const webID = document.getElementById("webID").value;

    if (webID === NOT_ENTERED_WEBID) {
        document.getElementById(
            "labelFN"
        ).textContent = `Login first, or enter a WebID (any WebID!) to read from its profile`;
        return false;
    }

    try {
        new URL(webID);
    } catch (_) {
        document.getElementById(
            "labelFN"
        ).textContent = `Provided WebID [${webID}] is not a valid URL - please try again`;
        return false;
    }

    const profileDocumentUrl = new URL(webID);
    profileDocumentUrl.hash = "";


    // Profile is public data; i.e., you do not need to be logged in to read the data.
    // For illustrative purposes, shows both an authenticated and non-authenticated reads.

    let myDataset;
    try {
        if (session.info.isLoggedIn) {
            myDataset = await getSolidDataset(profileDocumentUrl.href, { fetch: session.fetch });
        } else {
            myDataset = await getSolidDataset(profileDocumentUrl.href);
        }
    } catch (error) {
        document.getElementById(
            "labelFN"
        ).textContent = `Entered value [${webID}] does not appear to be a WebID. Error: [${error}]`;
        return false;
    }

    const profile = getThing(myDataset, webID);

    const formattedName = getStringNoLocale(profile, VCARD.fn);

    // Update the page with the retrieved values.
    document.getElementById("labelFN").textContent = `[${formattedName}]`;
}

async function getSession() {
    // Get the current session
    const session = getDefaultSession();
    // If the session exists and is not expired, return it
    if (session && session.info.isLoggedIn && !session.info.isExpired) {
        return session;
    }
    // Otherwise, try to login the user and return the session
    try {
        await session.login();
        return session;
    } catch (error) {
        console.log(`Error logging in: ${error}`);
        return null;
    }
}

// 4. Upload files

mainFolder.addEventListener("change", (event) => {
    handleRedirectAfterLogin();
    uploadAllinsideFolder(event);
});

async function uploadAllinsideFolder(event) {

    // var fileList = document.getElementById('fileList');
    var nomMedia = document.getElementById('mediaTypeU').value;


    // recup url pod
    const mypods = await getPodUrlAll(webID, { fetch: session.fetch });

    // console.log(`pods url : ${mypods}`);
    const url = `${mypods}private/`;
    const urlMedia = `${url}${nomMedia}/`;
    // console.log(`pods url : ${urlMedia}`);

    // const session2 = await login();

    await createContainerAt(urlMedia, { fetch: session.fetch });

    // fileList.innerHTML = ''; // Efface la liste précédente

    var files = event.target.files;
    if (files.length > 0) {

        Array.from(files).forEach(async function (file) {

            var filePath = file.webkitRelativePath;
            var parts = filePath.split('/');
            var fileName = parts.pop(); // Nom du fichier
            var nomDossierParent = parts[0];

            // creation conteneur du media
            var fileDirectory = parts.join('/'); // Chemin du dossier

            var listItem = document.createElement('li');
            listItem.textContent = fileDirectory + '/';
            fileDirectory = fileDirectory.replace(nomDossierParent, nomMedia);
            // console.log(`file directory: ${fileDirectory.replace(nomDossierParent, nomMedia)}`);

            // creation des conteneurs du fichiers en question

            var splitFileDirectory = listItem.textContent.split('/');
            // console.log(`split: ${splitFileDirectory}  taille : ${splitFileDirectory.length}`);
            var urlSt = `${urlMedia}`;
            var last2 = urlSt.slice(-2);

            if (last2 == `//`) {
                urlSt = urlSt.replace(/.$/, '');
            }

            for (const st of splitFileDirectory) {

                if (st != nomMedia && st != splitFileDirectory[0]) {
                    urlSt = `${urlSt}${st}/`
                    var last2 = urlSt.slice(-2);

                    if (last2 == `//`) {
                        urlSt = urlSt.replace(/.$/, '');
                    }
                    // console.log(`urlSt : ${urlSt}`);
                    // console.log(`st : ${st} last2 ${last2}`);
                    await createContainerAt(urlSt, { fetch: session.fetch });
                }

            }
            // fileList.appendChild(listItem);
            // console.log(`urlSt end : ${urlSt}`);

            var fileItem = document.createElement('li');
            fileItem.textContent = '  - ' + fileName;
            // console.log(`filename: ${fileName}`);
            await uploadFile2(file, urlSt, filePath);
            // fileList.appendChild(fileItem);
        });
    }

}

async function uploadFile2(file, originUrl, filePath) {

    // console.log(`pods url : ${originUrl}`);
    // console.log(`file : ${file.name}`);
    // console.log(`type : ${typeof file}`);
    var parts = filePath.split('/');
    var fileName = parts.pop();
    const fileExtension = fileName.split('.').pop();
    // console.log(`fileExtension : ${fileExtension}`);
    const url = `${originUrl}${fileName}`;
    try {
        const fileResponse = await saveFileInContainer(`${originUrl}`, file, { slug: fileName, contentType: fileExtension, fetch: session.fetch });
        document.getElementById(
            "labelUploadStatus"
        ).innerHTML = `File uploaded successfully at <a href="${url}" target="_blank">${url}</a>`;
        document.getElementById("labelUploadStatus").setAttribute("role", "alert");
    } catch (error) {
        console.log(`Error uploading file: ${error}`);
        document.getElementById(
            "labelUploadStatus"
        ).innerHTML = `Error uploading file: ${error}`;
        document.getElementById("labelUploadStatus").setAttribute("role", "alert");
    }
}


async function uploadFile(event, fileInput) {
    event.preventDefault();
    const mediaTypeU = document.getElementById("mediaTypeU");

    const fileName = fileInput.files[0].name;
    const fileExtension = fileName.split('.').pop();

    const file = fileInput.files[0];
    const mypods = await getPodUrlAll(webID, { fetch: fetch });
    console.log(`pods url : ${mypods}`);
    const url = `${mypods}public/${file.name}`;
    try {
        const fileResponse = await saveFileInContainer(`${mypods}public/`, file, { slug: fileName, contentType: fileExtension, fetch: session.fetch });
        document.getElementById(
            "labelUploadStatus"
        ).innerHTML = `File uploaded successfully at <a href="${url}" target="_blank">${url}</a>`;
        document.getElementById("labelUploadStatus").setAttribute("role", "alert");
    } catch (error) {
        console.log(`Error uploading file: ${error}`);
        document.getElementById(
            "labelUploadStatus"
        ).innerHTML = `Error uploading file: ${error}`;
        document.getElementById("labelUploadStatus").setAttribute("role", "alert");
    }
}

// Get the list of folders.
async function getFolders() {

    const webID = document.getElementById("webID").value;
    const podUrl = webID.split("/").slice(0, 3).join("/");
    console.log(podUrl);

    const myDataset = await getSolidDataset(podUrl, { fetch: session.fetch });
    console.log(myDataset);
    const myThings = getThingAll(myDataset);
    console.log(myThings);
    const folders = myThings.filter(thing => thing.url.endsWith("/")).map(folder => folder.url);
    return folders;

}

// Display the list of folders.
async function displayFolders() {
    const folders = await getFolders();
    console.log(folders);
    const foldersList = document.getElementById("foldersList");
    if (folders.length === 0) {
        foldersList.innerHTML = "You have no folders.";
    } else {
        foldersList.innerHTML = "Your folders:";
        const ul = document.createElement("ul");
        folders.forEach(folder => {
            const li = document.createElement("li");
            li.textContent = folder;
            ul.appendChild(li);
        });
        foldersList.appendChild(ul);
    }
}

async function getAllInsideFolder(typeMedia) {
    const webID = document.getElementById("webID").value;
    const podUrl = webID.split("/").slice(0, 3).join("/");
    const podUrlWithPublic = podUrl + "/private/" + typeMedia + "/";

    const myDataset = await getSolidDataset(podUrlWithPublic, { fetch: session.fetch });
    const myThings = getThingAll(myDataset);

    const links = [];

    let isFirstElement = true;

    for (const thing of myThings) {
        if (isFirstElement) {
            isFirstElement = false; // Ignorer le premier élément
            continue;
        }

        const thingUrl = thing.url;

        // links.push(thingUrl);

        // Vérifier si le lien se termine par "/"
        if (thingUrl.endsWith("/")) {
            const folderContents = await getAllInsideFolderRecursive(thingUrl);
            links.push(...folderContents);
        } else {
            links.push(thingUrl);
        }

    }

    return links;
}

async function getAllInsideUnderFolder(folderUrl) {
    const mypods = await getPodUrlAll(webID, { fetch: session.fetch });
    const myDataset = await getSolidDataset(folderUrl, { fetch: session.fetch });
    const myThings = getThingAll(myDataset);

    const links = [];

    let isFirstElement = true;

    for (const thing of myThings) {
        if (isFirstElement) {
            isFirstElement = false; // Ignorer le premier élément
            continue;
        }

        const thingUrl = thing.url;

        // links.push(thingUrl);

        // Vérifier si le lien se termine par "/"
        if (thingUrl.endsWith("/")) {
            const folderContents = await getAllInsideFolderRecursive(thingUrl);
            links.push(...folderContents);
        } else {
            links.push(thingUrl);
        }

    }

    return links;
}

async function getAllInsideUnderWithoutUnexpectedFolder(folderUrl) {
    const mypods = await getPodUrlAll(webID, { fetch: session.fetch });
    const myDataset = await getSolidDataset(folderUrl, { fetch: session.fetch });
    const myThings = getThingAll(myDataset);

    const links = [];

    let isFirstElement = true;

    for (const thing of myThings) {
        if (isFirstElement) {
            isFirstElement = false; // Ignorer le premier élément
            continue;
        }

        const thingUrl = thing.url;

        // links.push(thingUrl);

        // Vérifier si le lien se termine par "/"
        if (thingUrl.endsWith("/")) {
            const folderContents = await getAllInsideUnderWithoutUnexpectedFolder(thingUrl);
            links.push(...folderContents);
        } else if(thingUrl.endsWith(".json")){
            links.push(thingUrl);
        }

    }

    return links;
}
async function getAllInsideFolderRecursive(folderUrl) {
    const myDataset = await getSolidDataset(folderUrl, { fetch: session.fetch });
    const myThings = getThingAll(myDataset);

    const links = [];

    let isFirstElement = true;

    for (const thing of myThings) {
        const thingUrl = thing.url;

        if (isFirstElement) {
            isFirstElement = false; // Ignorer le premier élément
            continue;
        }

        // Vérifier si le lien se termine par "/"
        if (thingUrl.endsWith("/")) {
            const folderContents = await getAllInsideFolderRecursive(thingUrl);
            links.push(...folderContents);
        } else {
            links.push(thingUrl);
        }
    }

    return links;
}

async function getAllFromPod() {
    // Code existant avant l'appel à getAllInsideFolder

    try {
        const typeMedia = document.getElementById("mediaType").value;
        const folders = await getAllInsideFolder(typeMedia);

        // console.log("toute l'arbo :");
        // for (const folder of folders) {
        //     console.log(folder);
        // }

        const foldersList = document.getElementById("filesDiv");
        if (folders.length === 0) {
            foldersList.innerHTML = "You have no files or sub folders.";
        } else {
            foldersList.innerHTML = "Your things:";
            const ul = document.createElement("ul");
            folders.forEach(folder => {
                const li = document.createElement("li");
                li.textContent = folder;
                ul.appendChild(li);
            });
            foldersList.appendChild(ul);
        }

    } catch (error) {
        console.error("Une erreur s'est produite :", error);
    }

    // Code existant après l'appel à getAllInsideFolder
}

async function finUpload(event) {
    await uploadAllinsideFolder(event);

    console.log(`FIN`);

    document.getElementById(
        "labelUploadStatus"
    ).innerHTML = `Fin Upload`;
    document.getElementById("labelUploadStatus").setAttribute("role", "alert");
}

buttonLogin.onclick = function () {
    login();

};

writeForm.addEventListener("submit", (event) => {
    event.preventDefault();
    writeProfile();
});

readForm.addEventListener("submit", (event) => {
    event.preventDefault();
    readProfile();
});

foldersForm.addEventListener("submit", (event) => {
    event.preventDefault();
    displayFolders();
});

filesForm.addEventListener("submit", (event) => {
    event.preventDefault();

    getAllFromPod();
});

uploadForm.addEventListener("submit", (event) => {
    event.preventDefault();
    finUpload(event);
});

const selectElementC = document.getElementById('mediaTypeC');

// Ajoutez un écouteur d'événement pour détecter le changement de valeur de la liste déroulante
selectElementC.addEventListener('change', async function () {
    const selectedValueC = selectElementC.value;
    await fetchAndDisplayFileContent(selectedValueC);
    await displayFileOnCalendar(selectedValueC);
});

const selectElement = document.getElementById('mediaTypeT');
    // Ajoutez un écouteur d'événement pour détecter le changement de valeur de la liste déroulante
selectElement.addEventListener('change', function() {
    
    const instaDiv = document.getElementById('infoInsta');
    const googleDiv = document.getElementById('infoGoogle');
    const fbDiv = document.getElementById('infoFacebook');

    const selectedValue = selectElement.value;

    // Masquez tous les div d'abord
    instaDiv.style.display = 'none';
    googleDiv.style.display = 'none';
    fbDiv.style.display = 'none';
    console.log(selectedValue);

    getFileProfil(selectedValue);

    // Affichez le div correspondant à l'élément sélectionné
    if (selectedValue === 'instagram') {
        instaDiv.style.display = 'block';
        
    } else if (selectedValue === 'google') {
        
        const carousel = document.getElementById('infoGoogle_carousel');
        const prevButton = document.getElementById('prev-button');
        const nextButton = document.getElementById('next-button');

        let currentIndex = 0;
        const images = Array.from(carousel.children);

        function moveToSlide(index) {
        carousel.style.transform = `translateX(-${index * 100}%)`;
        }

        prevButton.addEventListener('click', () => {
        currentIndex = (currentIndex - 1 + images.length) % images.length;
        moveToSlide(currentIndex);
        });

        nextButton.addEventListener('click', () => {
        currentIndex = (currentIndex + 1) % images.length;
        moveToSlide(currentIndex);
        });

        googleDiv.style.display = 'block';
    } else if (selectedValue == `facebook`){
        fbDiv.style.display = 'block';
    }
});

function convertTimestampToDateTime(timestamp) {
    const date = new Date(timestamp * 1000); // Convertir le timestamp en millisecondes
  
    const day = date.getDate().toString().padStart(2, '0'); // Jour (format deux chiffres)
    const month = (date.getMonth() + 1).toString().padStart(2, '0'); // Mois (format deux chiffres)
    const year = date.getFullYear().toString(); // Année
    const hours = date.getHours().toString().padStart(2, '0'); // Heures (format deux chiffres)
    const minutes = date.getMinutes().toString().padStart(2, '0'); // Minutes (format deux chiffres)
  
    const formattedDate = `${day}/${month}/${year} ${hours}:${minutes}`;
  
    return formattedDate;
}

async function getFileProfil(typeMedia){
    try {
        // console.log(session);
        // console.log(typeMedia);
        
        const mypods = await getPodUrlAll(webID, { fetch: session.fetch });

        // console.log(`pod url : ${mypods}`);

        if(typeMedia == `instagram`){
            const fileUrlI1 = `${mypods}private/${typeMedia}/personal_information/account_information.json`;
            const fileUrlI2 = `${mypods}private/${typeMedia}/personal_information/linked_meta_accounts.json`;
            const fileUrlI3 = `${mypods}private/${typeMedia}/personal_information/personal_information.json`;
            const fileUrlI4 = `${mypods}private/${typeMedia}/personal_information/profile_changes.json`;    
            
            var fileUrls = [];
            fileUrls.push(fileUrlI1);
            fileUrls.push(fileUrlI2);
            fileUrls.push(fileUrlI3);
            fileUrls.push(fileUrlI4);

            var infoInstagram = [[]];

            for(const urli of fileUrls){
                console.log(urli);
                const file = await getFile(urli, {fetch : session.fetch});
                // console.log(file);
                var lastLogin;
                var lastLogout;
                var firstStoryTime;
                var lastStoryTime;

                var accountType1;
                var userName1;
                var email1;
                var phoneNumber1;

                var accountType2;
                var userName2;
                var email2;
                var phoneNumber2;

                var profilePhoto;
                var name;
                var bio;
                var gender;
                const imgElement = document.createElement('img');
                const photoContainer = document.getElementById('infoInsta_photo');
                var imgUrl;

                var profileChangeLastDate;

                const userNameD = document.getElementById('infoInsta_UserName');
                const nameD = document.getElementById('infoInsta_Name');
                const bioD = document.getElementById('infoInsta_Bio');
                const genderD = document.getElementById('infoInsta_Gender');
                const emailD = document.getElementById('infoInsta_Email');
                const phoneD = document.getElementById('infoInsta_Phone');

                const lastChangeD = document.getElementById('infoInsta_LastChange');
                const LastLoginD = document.getElementById('infoInsta_LastLogin');
                const LastLogoutD = document.getElementById('infoInsta_LastLogout');
                const FirstStoryTimeD = document.getElementById('infoInsta_FirstStoryTime');
                const LastStoryTimeD = document.getElementById('infoInsta_LastStoryTime');

                file.text().then(async (contenuTexte) => {
                    // console.log(contenuTexte); 
              
                      const fichierJSON = JSON.parse(contenuTexte);
                      console.log(fichierJSON);

                      if(urli == fileUrlI1){
                        const insights = fichierJSON.profile_account_insights[0].string_map_data;

                        lastLogin = convertTimestampToDateTime(insights["Last Login"].timestamp);
                        lastLogout = convertTimestampToDateTime(insights["Last Logout"].timestamp);
                        firstStoryTime = convertTimestampToDateTime(insights["First Story Time"].timestamp);
                        lastStoryTime = convertTimestampToDateTime(insights["Last Story Time"].timestamp);

                        LastLoginD.textContent = `Dernière connexion : ${lastLogin}`;
                        LastLogoutD.textContent = `Dernière déconnexion : ${lastLogout}`;
                        FirstStoryTimeD.textContent = `Première story : ${firstStoryTime}`;
                        LastStoryTimeD.textContent = `Dernière story : ${lastStoryTime}`;

                      } else if(urli == fileUrlI2){
                        const meta_data = fichierJSON.profile_linked_meta_accounts;
                        
                        accountType1 = meta_data[0].string_map_data["Account Type"].value;
                        userName1 = meta_data[0].string_map_data["User Name"].value;
                        email1 = meta_data[0].string_map_data.Email.value;
                        phoneNumber1 = meta_data[0].string_map_data["Phone Number"].value;

                        accountType2 = meta_data[1].string_map_data["Account Type"].value;
                        userName2 = meta_data[1].string_map_data["User Name"].value;
                        email2 = meta_data[1].string_map_data.Email.value;
                        phoneNumber2 = meta_data[1].string_map_data["Phone Number"].value;

                        userNameD.textContent = `Nom utilisateur  : ${userName2}`;
                        emailD.textContent = `Email : ${email2}`;
                        phoneD.textContent = `Téléphone : ${phoneNumber2}`;

                      } else if (urli == fileUrlI3){
                        const personal = fichierJSON.profile_user[0];
                        
                        profilePhoto = personal.media_map_data["Profile Photo"].uri;
                        name = personal.string_map_data.Name.value;
                        
                        bio = stripNonPrintableAndNormalize(personal.string_map_data.Bio.value);
                        gender = personal.string_map_data.Gender.value;
                        // console.log(profilePhoto);

                        const profilePhotoUrl = `${mypods}private/${typeMedia}/${profilePhoto}`;
                        const photoI = await getFile(profilePhotoUrl, {fetch : session.fetch});
                        imgUrl = URL.createObjectURL(photoI);
                        imgElement.src = imgUrl;

                        nameD.textContent = `Nom : ${name}`;
                        bioD.textContent = `Bio : ${bio}`;
                        genderD.textContent = gender == `female` ? `Sexe : femme` : `Sexe : homme`;


                        photoContainer.appendChild(imgElement);
                      } else if (urli == fileUrlI4){
                        profileChangeLastDate = convertTimestampToDateTime(fichierJSON.profile_profile_change[0].string_map_data["Change Date"].timestamp);
                        lastChangeD.textContent = `Dernière changement de profil : ${profileChangeLastDate}`;
                      }
        
                });
            }
                
        } else if(typeMedia == `google`){

            const fileUrlG = `${mypods}private/${typeMedia}/Profil/Profil.json`;
            const file = await getFile(fileUrlG, {fetch : session.fetch});
            console.log(file);

            const fileUrlGP = `${mypods}private/${typeMedia}/Profil/Photo_de_profil.jpg`;
            const fileP = await getFile(fileUrlGP, {fetch : session.fetch});
            console.log(fileP);

            const imgElement = document.createElement('img');
            const photoContainer = document.getElementById('infoGoogle_photo');

            
            const imgUrl = URL.createObjectURL(fileP);
            imgElement.src = imgUrl;
            photoContainer.appendChild(imgElement);


            file.text().then(async (contenuTexte) => {
                // console.log(contenuTexte); 
          
                const fichierJSON = JSON.parse(contenuTexte);
                // console.log(fichierJSON);
                const prenomD = document.getElementById('infoGoogle_prenom');
                const nomD = document.getElementById('infoGoogle_nom');
                const emailsD = document.getElementById('infoGoogle_emails');
                const dateD = document.getElementById('infoGoogle_date');
                const sexeD = document.getElementById('infoGoogle_sexe');
                const anciennesPhotos = document.getElementById('infoGoogle_carousel');

                console.log(fichierJSON.name.givenName);

                prenomD.textContent = `Prénom du profil : ${fichierJSON.name.givenName}`;
                nomD.textContent = `Nom du profil : ${fichierJSON.name.familyName}`;
                const dateStr = fichierJSON.birthday;
                const dateObj = new Date(dateStr);
                const formattedDate = `${dateObj.getDate()}-${dateObj.getMonth() + 1}-${dateObj.getFullYear()}`;

                dateD.textContent = `Date de naissance : ${formattedDate}`;
                sexeD.textContent = fichierJSON.gender.type == `female` ? `Sexe : femme` : `Sexe : homme`;

                const fileUrlGP = `${mypods}private/${typeMedia}/Profil/PhotosDuProfil/`;

                const myDataset = await getSolidDataset(fileUrlGP, { fetch: session.fetch });
                const myThings = getThingAll(myDataset);

                const emails = fichierJSON.emails;
                for(const el of emails) {
                    const descriptionText = document.createElement('p');
                    descriptionText.textContent = el.value;
                    console.log(el);
                    emailsD.appendChild(descriptionText);
                }
            
                const links = [];

                let isFirstElement = true;
            
                for (const thing of myThings) {
                    if (isFirstElement) {
                        isFirstElement = false; // Ignorer le premier élément
                        continue;
                    }

                    const thingUrl = thing.url;

                    // links.push(thingUrl);
            
                    // Vérifier si le lien se termine par "/"
                    if (thingUrl.endsWith("/")) {
                        const folderContents = await getAllInsideFolderRecursive(thingUrl);
                        links.push(...folderContents);
                    } else{
                        links.push(thingUrl);
                    }
                }
                
                for(const lk of links){
                    const imgElementLk = document.createElement('img');
                    const filePLk = await getFile(lk, {fetch : session.fetch});
                    const imgUrlLk = URL.createObjectURL(filePLk);
                    imgElementLk.src = imgUrlLk;
                    anciennesPhotos.appendChild(imgElementLk);
                }

            });
        } else if(typeMedia == `facebook`){

            const fileUrlF = `${mypods}private/${typeMedia}/profile_information/profile_information.json`;
            const file = await getFile(fileUrlF, {fetch : session.fetch});


            file.text().then(async (contenuTexte) => {
                // console.log(contenuTexte); 
          
                const fichierJSON = JSON.parse(contenuTexte);
                console.log(fichierJSON);

                const fullNameD = document.getElementById('infoFacebook_fullName');
                const emailsD = document.getElementById('infoFacebook_emails');
                const dateD = document.getElementById('infoFacebook_date');
                const sexeD = document.getElementById('infoFacebook_sexe');
                const currentCityD = document.getElementById('infoFacebook_currentCity');
                const hometownD = document.getElementById('infoFacebook_hometown');
                const educationsD = document.getElementById('infoFacebook_educations');
                const phoneD = document.getElementById('infoFacebook_phones');
                const livedD = document.getElementById('infoFacebook_lived');

                const fb = fichierJSON.profile_v2;

                fullNameD.textContent = `Nom du profil : ${fb.name.full_name}`;
                const formattedDate = `${fb.birthday.day}/${fb.birthday.month}/${fb.birthday.year}`

                dateD.textContent = `Date de naissance : ${formattedDate}`;
                sexeD.textContent = fb.gender.gender_option == `FEMALE` ? `Sexe : femme` : `Sexe : homme`; 
                
                const emails = fb.emails.emails;
                for(const el of emails) {
                    const descriptionText = document.createElement('p');
                    descriptionText.textContent = el;
                    console.log(el);
                    emailsD.appendChild(descriptionText);
                }

                currentCityD.textContent = `Vie à : ${fb.current_city.name}`;
                hometownD.textContent = `Originaire de : ${fb.hometown.name}`;

                const educationsE = fb.education_experiences;
                for(const ed of educationsE) {
                    const descriptionText = document.createElement('p');
                    descriptionText.textContent = stripNonPrintableAndNormalize(ed.name);
                    educationsD.appendChild(descriptionText);
                }

                const phoneNumbersE = fb.phone_numbers;
                for(const ph of phoneNumbersE) {
                    const descriptionText = document.createElement('p');
                    descriptionText.textContent = ph.phone_number;
                    phoneD.appendChild(descriptionText);
                }

                const placesE = fb.places_lived;
                for(const pl of placesE) {
                    const descriptionText = document.createElement('p');
                    descriptionText.textContent = stripNonPrintableAndNormalize(pl.place);
                    livedD.appendChild(descriptionText);
                }

            });
        }
    
    } catch (error) {
      console.error(error);
    }
}


const selectElementDe = document.getElementById('mediaTypeDe');
    // Ajoutez un écouteur d'événement pour détecter le changement de valeur de la liste déroulante
    selectElementDe.addEventListener('change', function() {
    
    const instaDiv = document.getElementById('instagram_de');
    const googleDiv = document.getElementById('google_de');
    const fbDiv = document.getElementById('facebook_de');

    const selectedValue = selectElementDe.value;

    // Masquez tous les div d'abord
    instaDiv.style.display = 'none';
    googleDiv.style.display = 'none';
    fbDiv.style.display = 'none';

    getDevices(selectedValue);

    // Affichez le div correspondant à l'élément sélectionné
    if (selectedValue === 'instagram') {
        instaDiv.style.display = 'block';
        
    } else if (selectedValue === 'google') {
        
        googleDiv.style.display = 'block';
    } else if (selectedValue == `facebook`){
        fbDiv.style.display = 'block';
    }
});

async function getDevices(typeMedia){
    const mypods = await getPodUrlAll(webID, { fetch: session.fetch });


    if(typeMedia == `google`){
        const googleDiv = document.getElementById('google_de');

        const fileUrlG1 = `${mypods}private/${typeMedia}/Activit%C3%A9%20du%20journal%20des%20acc%C3%A8s/Activit%C3%A9s%C2%A0_%20liste%20des%20services%C2%A0Google%20auxquels%20vos.csv`;
        const fileUrlG2 = `${mypods}private/${typeMedia}/Activit%C3%A9%20du%20journal%20des%20acc%C3%A8s/Appareils%C2%A0_%20liste%20des%20appareils%20(par%20exemple%2C%20Nest.csv`;

        const services = await getFile(fileUrlG1, {fetch : session.fetch});
        const devices = await getFile(fileUrlG2, {fetch : session.fetch});


        services.text().then(async (contenuTexte) => {

            const lines = contenuTexte.split('\n');
            const data = [];
            
            for (let i = 1; i < lines.length; i++) {
              const columns = lines[i].split(',');
            
              const activityTimestamp = columns[1];
              const ipAddress = columns[2];
              const userAgentString = columns[8];
            
              data.push({
                activityTimestamp,
                ipAddress,
                userAgentString
              });
            }
            
            const serviceDiv = document.getElementById('service');
            
            const table = document.createElement('table');
            table.classList.add('interpretation-table');
            
            const tableHeader = document.createElement('thead');
            const headerRow = document.createElement('tr');
            const header0 = document.createElement('th');
            header0.textContent = 'Ligne';
            const header1 = document.createElement('th');
            header1.textContent = 'Dates activités';
            const header2 = document.createElement('th');
            header2.textContent = 'IP adresses';
            const header3 = document.createElement('th');
            header3.textContent = 'UserAgent';
            
            headerRow.appendChild(header0);
            headerRow.appendChild(header1);
            headerRow.appendChild(header2);
            headerRow.appendChild(header3);
            tableHeader.appendChild(headerRow);
            table.appendChild(tableHeader);
            
            const tableBody = document.createElement('tbody');
            for (let i = 0; i < data.length; i++) {
              const item = data[i];
              const row = document.createElement('tr');
            
              const cell0 = document.createElement('td');
              cell0.textContent = i + 1;
              row.appendChild(cell0);
            
              const cell1 = document.createElement('td');
              cell1.textContent = item.activityTimestamp;
              row.appendChild(cell1);
            
              const cell2 = document.createElement('td');
              cell2.textContent = item.ipAddress;
              row.appendChild(cell2);
            
              const cell3 = document.createElement('td');
              cell3.textContent = item.userAgentString;
              row.appendChild(cell3);
            
              tableBody.appendChild(row);
            }
            
            table.appendChild(tableBody);
            serviceDiv.appendChild(table);
            

            
        });

        devices.text().then(async (contenuTexte) => {

            const lines = contenuTexte.split('\n');
            const data = [];

            for (let i = 1; i < lines.length; i++) {
            const columns = lines[i].split(',');

            const deviceType = columns[0];
            const marketingName = columns[2];
            const os = columns[3];
            let deviceLastLocation = columns[7] || ''; 
            deviceLastLocation = deviceLastLocation.replace(/\r?\n|\r/g, '');

            data.push({
                deviceType,
                marketingName,
                os,
                deviceLastLocation
            });
            }

            const appareils = document.getElementById('appareils');

            const table = document.createElement('table');
            table.classList.add('interpretation-table');

            const tableHeader = document.createElement('thead');
            const headerRow = document.createElement('tr');
            const header1 = document.createElement('th');
            header1.textContent = 'Device Type';
            const header2 = document.createElement('th');
            header2.textContent = 'Marketing Name';
            const header3 = document.createElement('th');
            header3.textContent = 'OS';
            const header4 = document.createElement('th');
            header4.textContent = 'Device Last Location';

            headerRow.appendChild(header1);
            headerRow.appendChild(header2);
            headerRow.appendChild(header3);
            headerRow.appendChild(header4);
            tableHeader.appendChild(headerRow);
            table.appendChild(tableHeader);

            const tableBody = document.createElement('tbody');
            for (let i = 0; i < data.length; i++) {
            const item = data[i];
            const row = document.createElement('tr');

            const cell1 = document.createElement('td');
            cell1.textContent = item.deviceType;
            row.appendChild(cell1);

            const cell2 = document.createElement('td');
            cell2.textContent = item.marketingName;
            row.appendChild(cell2);

            const cell3 = document.createElement('td');
            cell3.textContent = item.os;
            row.appendChild(cell3);

            const cell4 = document.createElement('td');
            cell4.textContent = item.deviceLastLocation;
            row.appendChild(cell4);

            tableBody.appendChild(row);
            }

            table.appendChild(tableBody);
            appareils.appendChild(table);

        });


    } else if (typeMedia == `facebook`){
        const fbDiv = document.getElementById('facebook_de');
        
    } else if (typeMedia == `instagram`){
        const instaDiv = document.getElementById('instagram_de');

    }

}

const selectElementP = document.getElementById('mediaTypeP');
    // Ajoutez un écouteur d'événement pour détecter le changement de valeur de la liste déroulante

selectElementP.addEventListener('change', async function() {

    const selectedValueP = selectElementP.value;

    await affichagePhotos(selectedValueP);
    
});

async function getAllInsideFolderRecursiveWithoutMP4(folderUrl) {
    const myDataset = await getSolidDataset(folderUrl, { fetch: session.fetch });
    const myThings = getThingAll(myDataset);

    const links = [];

    let isFirstElement = true;

    for (const thing of myThings) {
        const thingUrl = thing.url;

        if (isFirstElement) {
            isFirstElement = false; // Ignorer le premier élément
            continue;
          }

        // Vérifier si le lien se termine par "/"
        if (thingUrl.endsWith("/")) {
            const folderContents = await getAllInsideFolderRecursiveWithoutMP4(thingUrl);
            links.push(...folderContents);
        } else if(thingUrl.endsWith(".jpg")) {
            links.push(thingUrl);
        } else if (thingUrl.endsWith(".mp4")) {

        }
        
    }

    return links;
}

async function affichagePhotos(typeMedia){

    const mypods = await getPodUrlAll(webID, { fetch: session.fetch });

    // const nameElement = document.createElement('p');
    // nameElement.textContent = 'Chargement des données';
    // affichagePhotos.appendChild(nameElement);

    if(typeMedia == `google`){
        const affichagePhotos = document.getElementById('affichage_photos');

        const fileUrlGP = `${mypods}private/${typeMedia}/Google%C2%A0Photos/`;

        const myDataset = await getSolidDataset(fileUrlGP, { fetch: session.fetch });
        const myThings = getThingAll(myDataset);
    
        const links = [];

        let isFirstElement = true;
    
        for (const thing of myThings) {
            if (isFirstElement) {
                isFirstElement = false; // Ignorer le premier élément
                continue;
            }

            const thingUrl = thing.url;

            // links.push(thingUrl);
    
            // Vérifier si le lien se termine par "/"
            if (thingUrl.endsWith("/")) {
                const folderName = thingUrl.split("/").slice(-2, -1)[0];

                console.log(folderName);

                // Vérifier si le nom du dossier correspond au pattern "Photos from ..."
                if (folderName.startsWith("Photos%20from%")) {
                    const folderContents = await getAllInsideFolderRecursive(thingUrl);
                    links.push(...folderContents);
                }

            } 
        }

        var images = [];
        
        for(let i = 0; i < links.length; i += 2){
            const imgElementLk = document.createElement('img');

            const fileI = await getFile(links[i], {fetch : session.fetch});
            const imgUrl = URL.createObjectURL(fileI);
            console.log(links[i]);

            const fileD = await getFile(links[i+1], {fetch : session.fetch});
            const descr = URL.createObjectURL(fileD);
            console.log(links[i+1]);

            fileD.text().then(async (contenuTexte) => {
                // console.log(contenuTexte); 
          
                const fichierJSON = JSON.parse(contenuTexte);
                const descrJ = `L'image a été créé le ${fichierJSON.creationTime.formatted} et a été modifié pour la dernière fois ${fichierJSON.photoLastModifiedTime.formatted}`;
                images.push({src: `${imgUrl}`, description : `${descrJ}`});

            });
            
            imgElementLk.src = imgUrl;
        }
          
        images.forEach((image) => {
            const imageContainer = document.createElement('div');
            imageContainer.classList.add('image-container');

            const imgElement = document.createElement('img');
            imgElement.src = image.src;

            const descriptionContainer = document.createElement('div');
            descriptionContainer.classList.add('description-container');

            const descriptionText = document.createElement('p');
            descriptionText.classList.add('description-text');
            descriptionText.textContent = image.description;

            descriptionContainer.appendChild(descriptionText);
            imageContainer.appendChild(imgElement);
            imageContainer.appendChild(descriptionContainer);

            affichagePhotos.appendChild(imageContainer);
        });

    } else if (typeMedia == `instagram`){

        const affichagePhotos = document.getElementById('affichage_photos');

        const fileUrlGI = `${mypods}private/${typeMedia}/media/`;

        const myDataset = await getSolidDataset(fileUrlGI, { fetch: session.fetch });
        const myThings = getThingAll(myDataset);
    
        const links = [];

        let isFirstElement = true;
    
        for (const thing of myThings) {
            if (isFirstElement) {
                isFirstElement = false; // Ignorer le premier élément
                continue;
            }

            const thingUrl = thing.url;

            // links.push(thingUrl);
    
            // Vérifier si le lien se termine par "/"
            if (thingUrl.endsWith("/")) {
                const folderContents = await getAllInsideFolderRecursiveWithoutMP4(thingUrl);
                links.push(...folderContents);
            } else if(thingUrl.endsWith(".jpg")){
                links.push(thingUrl);
            } else if (thingUrl.endsWith(".mp4")) {
            
            }
        }
        
        for(let i = 0; i < links.length; i ++){
            const imgElementLk = document.createElement('img');

            const fileI = await getFile(links[i], {fetch : session.fetch});
            const imgUrl = URL.createObjectURL(fileI);
            console.log(links[i]);
            imgElementLk.src = imgUrl;

            const imageContainer = document.createElement('div');
            imageContainer.classList.add('image-container');

            const imgElement = document.createElement('img');
            imgElement.src = imgUrl;
            
            imageContainer.appendChild(imgElement);
            
            affichagePhotos.appendChild(imageContainer);
        }

    }
    
}

const images = document.querySelectorAll('#affichage_photos img');

// Parcourir toutes les images et ajouter des écouteurs d'événements
images.forEach(image => {
  // Récupérer le conteneur de description correspondant à l'image
  const descriptionContainer = image.parentElement.querySelector('.description-container');

  // Au survol de la souris
  image.addEventListener('mouseover', () => {
    image.classList.add('hovered');
    descriptionContainer.classList.add('hovered');
  });

  // Lorsque la souris quitte l'image
  image.addEventListener('mouseout', () => {
    image.classList.remove('hovered');
    descriptionContainer.classList.remove('hovered');
  });
});


//Récupération d'un fichier a l'URL en paramètre sur un pod 
async function fetchAndDisplayFileContent(selectedValueC) {
    try {
        const mypods = await getPodUrlAll(webID, { fetch: session.fetch });
        var podUrlWithPublic;
        if (selectedValueC == "instagram") {
            podUrlWithPublic = mypods + "private/" + selectedValueC + "/" + "comments/";
        } else if (selectedValueC == "facebook") {
            podUrlWithPublic = mypods + "private/" + selectedValueC + "/" + "comments_and_reactions/";
        }else if(selectedValueC == "google"){
            podUrlWithPublic = mypods + "private/" + selectedValueC + "/" + "Mail/";
            return[];
        }
        const fileUrls = await getAllInsideUnderFolder(podUrlWithPublic);
        const fileDataArray = [];

        for (const fileUrl of fileUrls) {
            const file = await getFile(fileUrl, { fetch: session.fetch });
            const contenuTexte = await file.text();

            var last4 = fileUrl.slice(-4);
            var last3 = fileUrl.slice(-3);
            const typeFichier = file.type;

            if (last4 == 'json' || last4 == 'JSON') {
                const fichierJSON = JSON.parse(contenuTexte);
                fileDataArray.push(fichierJSON);
            } else if (last3 == 'csv' || last3 == 'CSV') {
                fileDataArray.push(contenuTexte);
            } else {
                console.log("Le fichier n'est pas au format JSON ou CSV");
            }
        }

        return fileDataArray;
    } catch (error) {
        console.error(error);
        return [];
    }
}

function stripNonPrintableAndNormalize(text) {
    // strip control chars
    text = text.replace(/\p{C}/gu, '');

    // other common tasks are to normalize newlines and other whitespace

    // normalize newline
    text = text.replace(/\n\r/g, '\n');
    text = text.replace(/\p{Zl}/gu, '\n');
    text = text.replace(/\p{Zp}/gu, '\n');

    // normalize space
    text = text.replace(/\p{Zs}/gu, ' ');

    return text;
}
async function transformCommentDataForCalendar(typeMedia) {
    try {
        const mypods = await getPodUrlAll(webID, { fetch: session.fetch });
        var podUrlWithPublic;
        if (typeMedia == "instagram") {
            podUrlWithPublic = mypods + "private/" + typeMedia + "/" + "comments/";
        } else if (typeMedia == "facebook") {
            podUrlWithPublic = mypods + "private/" + typeMedia + "/" + "comments_and_reactions/";

        }
        const fileUrls = await getAllInsideUnderFolder(podUrlWithPublic);
        const events = [];
        for (const fileUrl of fileUrls) {
            const fileData = await getFile(fileUrl, { fetch: session.fetch });
            if (typeMedia == "instagram") {
                fileData.text().then(async (contenuTexte) => {
                    const fichierJSON = JSON.parse(contenuTexte);
                    if (Array.isArray(fichierJSON)) {
                        let comments = fichierJSON;
                        // Check if fileData has {"comments_reels_comments": [...]}, and extract the comments array
                        if (fichierJSON.length === 1 && fichierJSON[0].comments_reels_comments) {
                            comments = fichierJSON[0].comments_reels_comments;
                        }

                        comments.forEach((item) => {
                            if (
                                item.string_map_data &&
                                item.string_map_data.Comment &&
                                item.string_map_data.Time &&
                                item.string_map_data.Time.timestamp
                            ) {
                                const comment = item.string_map_data.Comment.value;
                                const owner = item.string_map_data["Media Owner"]
                                    ? item.string_map_data["Media Owner"].value
                                    : "";

                                const event = {
                                    start: new Date(item.string_map_data.Time.timestamp * 1000),
                                    title:
                                        'Vous avez écrit ce commentaire : "' +
                                        stripNonPrintableAndNormalize(comment) +
                                        '" sous la publication de @' +
                                        owner,
                                };
                                events.push(event);
                            }
                        });
                    } else if (fichierJSON.comments_reels_comments) {
                        fichierJSON.comments_reels_comments.forEach((item) => {
                            if (
                                item.string_map_data &&
                                item.string_map_data.Comment &&
                                item.string_map_data.Time &&
                                item.string_map_data.Time.timestamp
                            ) {
                                const comment = item.string_map_data.Comment.value;
                                const owner = item.string_map_data["Media Owner"]
                                    ? item.string_map_data["Media Owner"].value
                                    : "";
                                const event = {
                                    start: new Date(item.string_map_data.Time.timestamp * 1000),
                                    title:
                                        'Vous avez écrit ce commentaire : "' +
                                        stripNonPrintableAndNormalize(comment) +
                                        '" dans la story de @' +
                                        owner,
                                };
                                events.push(event);
                            }
                        });
                    }
                });
            }

            if (typeMedia == "facebook") {
                const fileText = await fileData.text();
                const fichierJSON = JSON.parse(fileText);

                if (Array.isArray(fichierJSON)) {
                    fichierJSON.forEach((item) => {
                        if (
                            item.timestamp &&
                            item.data[0] &&
                            item.data[0].reaction &&
                            item.data[0].reaction.reaction &&
                            item.data[0].reaction.actor &&
                            item.title
                        ) {
                            const title = item.title;
                            const reaction = item.data[0].reaction.reaction;
                            const author = item.data[0].reaction.actor;
                            const event = {
                                start: new Date(item.timestamp * 1000),
                                title: author + ': à ' + reaction + '\n' + title,
                            };

                            events.push(event);
                        }
                    });
                }
            }
        }

        return events;

    } catch (error) {
        console.error(error);
        return [];
    }
}

const selectElementM = document.getElementById('mediaTypeM');
const convNameElement = document.getElementById('convName');
let messagesContainer = document.getElementById('messagesFb');
let conversations = [];// Ajoutez un écouteur d'événement pour détecter le changement de valeur de la liste déroulante

    convNameElement.addEventListener('change', function() {
        const selectedConversationName = convNameElement.value;
        const selectedConversation = conversations.find(conversation => conversation.conversationName === selectedConversationName);
        if (selectedConversation) {
            displayConversation(selectedConversation);
        }
    });
    

async function getFacebookMessage(selectedValueM) {
    try {
        const mypods = await getPodUrlAll(webID, { fetch: session.fetch });
        let podUrlWithPublic;
        if (selectedValueM === "facebook") {
            podUrlWithPublic = mypods + "private/" + selectedValueM + "/messages/inbox/";
        }
        const conversationUrls = await getAllInsideUnderWithoutUnexpectedFolder(podUrlWithPublic);
        const conversationsData = [];
        console.log("je suis dans get facebook");
        for (const conversationUrl of conversationUrls) {
            const conversation = await getFile(conversationUrl, { fetch: session.fetch });
            const conversationData = await conversation.text();

            // Parse the conversation data and extract the relevant information
            const conversationJSON = JSON.parse(conversationData);
            const participants = conversationJSON.participants;
            const messages = conversationJSON.messages.map(message => {
                return {
                    sender: message.sender_name,
                    content: message.content,
                    timestamp: new Date(message.timestamp_ms),
                };
            });

            const conversationInfo = {
                conversationName: getConversationName(conversationUrl),
                participants: participants,
                messages: messages,
            };
            console.log("je suis dans le for");
            console.log(conversationInfo.participants);
            conversationsData.push(conversationInfo);
        }
        console.log(conversationsData);
        return conversationsData;
    } catch (error) {
        console.error(error);
        return [];
    }
}

function getConversationName(conversationUrl) {
    const segments = conversationUrl.split("/");
    return segments[segments.length - 2]; // Le nom de la conversation est l'avant-dernier segment
}

async function displayConversation(selectedConversation) {
    // Affichez la conversation sur votre interface utilisateur
    console.log("displayConversation appelée avec la conversation :", selectedConversation);

    // Par exemple, vous pouvez créer une div pour afficher le contenu de la conversation
    messagesContainer.innerHTML = ''; // Réinitialisez le contenu de messagesContainer

    selectedConversation.messages.forEach(message => {
        const sender = message.sender;
        const content = message.content;
        const timestamp = message.timestamp;

        const messageElement = document.createElement('p');
        messageElement.textContent = `${sender}: ${content} (${timestamp})`;

        messagesContainer.appendChild(messageElement);
    });
}


async function updateConversationOptions() {
    const selectedValueM = selectElementM.value;
    const conversationNames = await getConversationNames(selectedValueM);
    convNameElement.innerHTML = ''; // Réinitialisez la liste déroulante
    conversationNames.forEach(name => {
        addOptionToSelect(name);
    });
}

function addOptionToSelect(conversationName) {
    const option = document.createElement('option');
    option.value = conversationName;
    option.text = conversationName;
    convNameElement.appendChild(option);
}

async function getConversationNames(selectedValueM) {
    const mypods = await getPodUrlAll(webID, { fetch: session.fetch });
    let podUrlWithPublic;
    if (selectedValueM === "facebook") {
        podUrlWithPublic = mypods + "private/" + selectedValueM + "/messages/inbox/";
    }
    const conversationUrls = await getAllInsideUnderWithoutUnexpectedFolder(podUrlWithPublic);
    const conversationNames = conversationUrls.map(conversationUrl => getConversationName(conversationUrl));
    return conversationNames;
}

// Ajoutez un écouteur d'événement pour détecter le changement de valeur de la liste déroulante
selectElementM.addEventListener('change', async function() {
    await updateConversationOptions();
    const selectedValueM = selectElementM.value; // Utilisez selectElementM au lieu de selectedValueM
    console.log(selectedValueM);
    conversations = await getFacebookMessage(selectedValueM); // Mettez à jour la variable conversations
    console.log("jesuis ici");
    console.log(conversations);
});

// Initialisez les options de conversation lors du chargement de la page
updateConversationOptions();


async function mboxToJson(selectedValueC) {
    try {
        const mypods = await getPodUrlAll(webID, { fetch: session.fetch });
        var podUrlWithPublic;
        if (selectedValueC == "google") {
            podUrlWithPublic = mypods + "private/" + selectedValueC + "/" + "Mail/";
            const fileUrls = await getAllInsideUnderFolder(podUrlWithPublic);

            var jsonMessages = [];

            for (const fileUrl of fileUrls) {
                const fileData = await getFile(fileUrl, { fetch: session.fetch });
                const mboxData = await fileData.text();
                var messages = mboxData.split('From ');
                messages.shift(); // Supprimer la première partie vide

                messages.forEach(function (message) {
                    var lines = message.split('\n');
                    var jsonMessage = {};

                    lines.forEach(function (line) {
                        if (line.startsWith('Date:')) {
                            jsonMessage.date = line.substring(6).trim();
                        } else if (line.startsWith('From:')) {
                            const fromMatch = line.match(/<([^>]+)>/);
                            if (fromMatch) {
                                jsonMessage.from = fromMatch[1];
                            }
                        } else if (line.startsWith('Subject:')) {
                            jsonMessage.subject = line.substring(9).trim();
                        } else if (line.startsWith('To:')) {
                            const toMatch = line.match(/<([^>]+)>/);
                            if (toMatch) {
                                jsonMessage.to = toMatch[1];
                            }
                        }
                    });
                    jsonMessages.push(jsonMessage);
                });
            }

            return jsonMessages;
        } else {
            return [];
        }
    } catch (error) {
        console.error(error);
        return [];
    }
}

async function transformGoogleMailForCalendar(jsonMessages) {
    try {
        if (!Array.isArray(jsonMessages)) {
            throw new Error("Invalid input. 'jsonMessages' should be an array.");
        }

        var events = [];
        jsonMessages.forEach(function (message) {
            var event = {
                title: "Mail reçu de : " + message.from + "\nSujet : "+ message.subject + "\n Envoyé à : "+ message.to,
                start: new Date(message.date),
            };

            events.push(event);
        });

        return events;
    } catch (error) {
        console.error(error);
        return [];
    }
}

async function displayFileOnCalendar(selectedValueC) {
    try {
        const fileData = await fetchAndDisplayFileContent(selectedValueC);
        var events;
        if(selectedValueC == "instagram" || selectedValueC =="facebook"){
            events = await transformCommentDataForCalendar(selectedValueC);
        }else if(selectedValueC == "google"){
            const jsonMessages = await mboxToJson(selectedValueC);
            events = await transformGoogleMailForCalendar(jsonMessages);
        }
        var calendarEl = document.getElementById('calendar');
        var calendar = new Calendar(calendarEl, {
            locale: 'fr',
            plugins: [interactionPlugin, dayGridPlugin, timeGridPlugin, listPlugin],
            headerToolbar: {
                left: 'prev,next today',
                center: 'title',
                right: 'dayGridMonth,timeGridWeek,timeGridDay,listWeek',
            },
            navLinks: true,
            editable: true,
            dayMaxEvents: true,
            events: events,
            eventRender: function (info) {
                const content = `
                    <div class="fc-event-dot" style="background-color: ${info.event.backgroundColor};"></div>
                    <div class="fc-event-title">${info.event.title}</div>
                `;
                const tooltip = `
                    <div class="fc-tooltip">
                        <div class="fc-tooltip-content">
                            <div class="fc-tooltip-title">${info.event.title}</div>
                            <div class="fc-tooltip-description">${info.event.extendedProps.description}</div>
                        </div>
                    </div>
                `;

                const eventElement = info.el.querySelector('.fc-event');
                tippy(eventElement, {
                    content: tooltip,
                    allowHTML: true,
                    theme: 'light',
                });

                info.el.innerHTML = content;

                eventElement.addEventListener('click', function () {
                    tippy.hideAll({ duration: 0 }); // Cacher tous les tooltips ouverts
                    tippy(eventElement, {
                        content: tooltip,
                        allowHTML: true,
                        theme: 'light',
                        placement: 'auto',
                        onShow(instance) {
                            instance.setContent(tooltip);
                        },
                    }).show();
                });
            },
        });

        calendar.setOption('locale', 'fr');
        calendar.render();
    } catch (error) {
        console.error(error);
    }
}
