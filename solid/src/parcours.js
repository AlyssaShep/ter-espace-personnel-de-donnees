const fs = require('fs');
const path = require('path');

function listFilesRecursively(directoryPath) {
  const files = fs.readdirSync(directoryPath);

  files.forEach((file) => {
    const filePath = path.join(directoryPath, file);
    const stats = fs.statSync(filePath);

    if (stats.isFile()) {
      console.log(filePath); // Affiche le chemin du fichier
    } else if (stats.isDirectory()) {
      console.log(filePath + path.sep); // Affiche le chemin du dossier
      listFilesRecursively(filePath); // Récursion pour parcourir les sous-dossiers
    }
  });
}

// Exemple d'utilisation : spécifiez le chemin du dossier à explorer
const directoryPath = 'instagram';
listFilesRecursively(directoryPath);
