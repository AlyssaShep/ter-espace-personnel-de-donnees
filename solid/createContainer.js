const { createContainerAt } = require("@inrupt/solid-client");

async function createEmptyFolder(podUrl, folderName) {
  try {
    const folderUrl = await createContainerAt(podUrl, folderName, {
      contentType: "text/turtle",
    });
    console.log(`Dossier créé à l'adresse ${folderUrl}`);
  } catch (error) {
    console.error("Erreur lors de la création du dossier :", error);
  }
}

// Exemple d'utilisation :
const podUrl = "https://luciesorreau.solidcommunity.net/public/";
const folderName = "mon-dossier";

createEmptyFolder(podUrl, folderName);

