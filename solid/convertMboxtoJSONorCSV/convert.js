const { exec } = require('child_process');

function convertMboxToJSON(mboxFileName, outputFileName) {
  // Commande pour convertir en JSON
  const jsonCommand = `mbox-to-json "${mboxFileName}" -o "${outputFileName}.json"`;

  exec(jsonCommand, (error, stdout, stderr) => {
    if (error) {
      console.error(`Erreur lors de la conversion en JSON : ${error.message}`);
      // En cas d'erreur, exécuter la commande pour convertir en CSV
      convertMboxToCSV(mboxFileName, outputFileName);
      return;
    }
    if (stderr) {
      console.error(`Erreur de sortie lors de la conversion en JSON : ${stderr}`);
      // En cas d'erreur, exécuter la commande pour convertir en CSV
      convertMboxToCSV(mboxFileName, outputFileName);
      return;
    }
    console.log(`Conversion en JSON terminée. Fichier de sortie : ${outputFileName}.json`);
  });
}

function convertMboxToCSV(mboxFileName, outputFileName) {
  // Commande pour convertir en CSV
  const csvCommand = `mbox-to-json "${mboxFileName}" -c -o "${outputFileName}.csv"`;

  exec(csvCommand, (error, stdout, stderr) => {
    if (error) {
      console.error(`Erreur lors de la conversion en CSV : ${error.message}`);
      return;
    }
    if (stderr) {
      console.error(`Erreur de sortie lors de la conversion en CSV : ${stderr}`);
      return;
    }
    console.log(`Conversion en CSV terminée. Fichier de sortie : ${outputFileName}.csv`);
  });
}

// Exemple d'utilisation
const mboxFileName = 'mail.mbox';
const outputFileName = 'mailOut';

convertMboxToJSON(mboxFileName, outputFileName);

